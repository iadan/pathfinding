/**
 * 
 */
package com.example.pathfinding.models;


import java.util.LinkedList;
import java.util.List;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;


public final class Grid2DA {

	public static final String TAG = Grid2DA.class.getSimpleName();

	private final int gridPixelWidth, gridPixelHeight;
	private final Cell [][] CELLS;
	private final int ROWS, COLS;
	
	private final Paint paint;

	public Grid2DA(int gridWidth, int gridHeight) {
		this.gridPixelWidth =gridWidth;
		this.gridPixelHeight = gridHeight;
		ROWS = (gridWidth/Renderable.RENDERABLE_WIDTH);		
		COLS = (gridHeight/Renderable.RENDERABLE_HEIGHT);
		paint = new Paint();
		CELLS = new Cell[ROWS][COLS];	
		setUpComponents();
	}
		
	
	private void setUpComponents() {
		// Set Up Paint Components
		paint.setColor(Color.BLUE);
		paint.setStyle(Paint.Style.STROKE);
		
		int i = 0; int j = 0;
		for (int x = 0; i < ROWS; x += Renderable.RENDERABLE_WIDTH) {
			for (int y = 0; j < COLS; y +=Renderable.RENDERABLE_HEIGHT) {
				CELLS[i][j] = new Cell(x, y, false);
				j++;
			}
			j = 0;
			i++;
		}
	}
	
	public final void draw2DGrid(Canvas canvas) {
		for(int x= 0; x<ROWS; x++){
			for(int y = 0; y<COLS; y++){
					canvas.drawRect(CELLS[x][y].getCellRectangle(), paint);
			}
		}
	}
	
	
	/**
	 * Generate Getters and Setters
	 */
	
	/**
	 * @return the gridPixelWidth
	 */
	public int getGridPixelWidth() {
		return gridPixelWidth;
	}


	/**
	 * @return the gridPixelHeight
	 */
	public int getGridPixelHeight() {
		return gridPixelHeight;
	}
	
	/**
	 * @return the rOWS
	 */
	public int getROWS() {
		return ROWS;
	}



	/**
	 * @return the cOLS
	 */
	public int getCOLS() {
		return COLS;
	}

	/**
	 * @return the cELLS
	 */
	public Cell[][] getCELLS() {
		return CELLS;
	}


	/**
	 * NEEDS TO BE IMPROVED WAY TOO BASIC!
	 * @param x
	 * @param y
	 * @return
	 */
	public Cell getCellAtLocation(int x, int y) {
		for (int i = 0; i < ROWS; i++) {
			for (int j = 0; j < COLS; j++) {
				if ((CELLS[i][j].getxPos() <= x && CELLS[i][j].getEndxPos() > x)
						&& (CELLS[i][j].getyPos() <= y && CELLS[i][j].getEndyPos() > y))
					return CELLS[i][j];
			}
		}
		return null;
	}
	



	public void setUpAdjacencies() {
		for (int i = 0; i < ROWS; i++) {
			for (int j = 0; j < COLS; j++) {
				List<Cell> neighbours = new LinkedList<Cell>();
				List<Cell> fourNeighbours = new LinkedList<Cell>();

				// check east
				if (j + 1 < COLS) {
					neighbours.add(CELLS[i][j + 1]);
					fourNeighbours.add(CELLS[i][j + 1]);
				}
				// check south east
				if (i + 1 < ROWS && j + 1 < COLS) {
					neighbours.add(CELLS[i + 1][j + 1]);
				}

				// check south
				if (i + 1 < ROWS) {
					neighbours.add(CELLS[i + 1][j]);
					fourNeighbours.add(CELLS[i + 1][j]);
				}
				// check south west{
				if (i + 1 < ROWS && j - 1 >= 0) {
					neighbours.add(CELLS[i + 1][j - 1]);
				}
				// check west
				if (j - 1 >= 0) {
					neighbours.add(CELLS[i][j - 1]);
					fourNeighbours.add(CELLS[i][j - 1]);
				}
				// check north west
				if (i - 1 >= 0 && j - 1 >= 0) {
					neighbours.add(CELLS[i - 1][j - 1]);
				}
				// check north
				if (i - 1 >= 0) {
					neighbours.add(CELLS[i - 1][j]);
					fourNeighbours.add(CELLS[i - 1][j]);
				}
				// check north east
				if (i - 1 >= 0 && j + 1 < COLS) {
					neighbours.add(CELLS[i - 1][j + 1]);
				}
				
				CELLS[i][j].setAllNeighbours(neighbours);
				CELLS[i][j].setFourNeighbours(fourNeighbours);
			}

		}
	}
	

	@Override
	public String toString() {
		String str = "[Grid2DA_Object:" + "rows= " + ROWS + ",Cols= " + COLS
				+ ",Total= " + ROWS * COLS + "]";
		return str;
	}

}
