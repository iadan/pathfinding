package com.example.pathfinding.activities;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.example.pathfinding.R;

public class SimulationActivity extends Activity {
	
	public static final String TAG = SimulationActivity.class.getSimpleName();
	
	private static DrawingPanel drawingPanel;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);	
		p("Simulation Activity Started");
		setContentView(R.layout.activity_simulation);
		
		
		drawingPanel = (DrawingPanel) findViewById(R.id.drawingPanel);

	}

	public void onBackPressed(){
		p("Back Pressed... Exiting");
		drawingPanel.stop();
		this.finish();
	}
	
	private void p(String message){
		Log.d(TAG, "Debug --> " + message);	
	}
}
